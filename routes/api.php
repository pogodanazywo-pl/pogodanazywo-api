<?php

use App\Models\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('/user')->group(
    function () {
        Route::post(
            '/register/',
            [\App\Http\Controllers\AuthController::class, 'register']
        );
        Route::post(
            '/login/',
            [\App\Http\Controllers\AuthController::class, 'login']
        );
        Route::middleware('auth:api')
            ->get(
                '/logout/',
                [\App\Http\Controllers\AuthController::class, 'logout']
            );
    });

Route::prefix('/sensors')->group(
    function () {
        Route::middleware('auth:api')->post('/store/', [\App\Http\Controllers\SensorController::class, 'store']);
        Route::middleware('auth:api')->delete('/delete/{sensor_id}', [\App\Http\Controllers\SensorController::class, 'delete']);
        Route::get('/', [\App\Http\Controllers\SensorController::class, 'getAll']);
        Route::get('/', [\App\Http\Controllers\SensorController::class, 'getAll']);
    });

Route::prefix('/measurements')->group(
    function () {
        Route::post('/store/{access_token}', [\App\Http\Controllers\MeasurementsController::class, 'store']);
        Route::get('/get/every-hour/{record_number}/sensor/{sensor_id}', [\App\Http\Controllers\MeasurementsController::class, 'getEveryHour']);
        Route::get('/get/every-day/{record_number}/sensor/{sensor_id}', [\App\Http\Controllers\MeasurementsController::class, 'getEveryDay']);
        Route::get('/get/now/sensor/{sensor_id}', [\App\Http\Controllers\MeasurementsController::class, 'getNow']);
        Route::get('/storeFake/{access_token}', [\App\Http\Controllers\MeasurementsController::class, 'storeFake']);
    });

Route::any('/unauthorized', function () {
    $response = new Response();
    $response->push_error('unauthorized_error');
    return $response->get();
})->name('unauthorized');
