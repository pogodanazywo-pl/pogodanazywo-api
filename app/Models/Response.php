<?php


namespace App\Models;


class Response
{
    private $error;
    private $data;

    public function __construct()
    {
        $this->error=[];
        $this->data=[];
    }

    public function push_error($error_message, $key=null){
        if($key==null){
            array_push($this->error, $error_message);
        }
        else{
            $this->error[$key]=$error_message;
        }
    }

    public function push_data($data, $key=null){
        if($key==null){
            array_push($this->data, $data);
        }
        else{
            $this->data[$key]=$data;
        }
    }

    public function get(){
        return json_encode([
            'error'=>$this->error,
            'data'=>$this->data
        ]);
    }
}
