<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    use HasFactory;

    public $temperature;
    public $humidity;
    public $pressure;
    public $measurementTime;

    public function __construct()
    {
    }

    public function fromArray($array){
        $this->temperature=$array['temperature'];
        $this->humidity=$array['humidity'];
        $this->pressure=$array['pressure'];
        $this->measurementTime=$array['measurement_time'];
    }

    public function toArray(){
        return [
            'measurement_time'=>$this.$this->measurementTime,
            'temperature'=>$this.$this->temperature,
            'humidity'=>$this.$this->humidity,
            'pressure'=>$this.$this->pressure
        ];
    }
}
