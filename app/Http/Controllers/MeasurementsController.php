<?php

namespace App\Http\Controllers;

use App\Models\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MeasurementsController extends Controller
{
    public function store(Request $request, $accessToken)
    {
        $correctData = true;
        $errors=array();

        $validationRules = array(
            'temperature' => 'required|numeric|min:-50|max:50',
            'humidity' => 'required|numeric|min:0|max:100',
            'pressure' => 'required|numeric|min:800|max:1100',
            'measurement_time' => 'required|numeric'
        );


        foreach ($request['data'] as $record){
            $validationResult = Validator::make($record, $validationRules);
            if ($validationResult->fails()) {
                $correctData = false;
                array_push($errors, 'data_validation_error');
            }
        }

        $sensor = DB::table('sensors')->where('access_token', $accessToken)->get()->last();
        if (is_null($sensor)) {
            $correctData = false;
            array_push($errors, 'sensor_find_error');
        }

        if ($correctData) {
            $sensorMasLevel=DB::table('sensors')->where('id', $sensor->id)->get('mas_level')->last();

            $insertData=array();

            foreach($request->input('data') as $record){
                $normalizedPressure = $record['pressure'] + (11.3 * $sensorMasLevel->mas_level / 100);
                $preparedData = array(
                    'temperature' => round($record['temperature'], 2),
                    'humidity' => round($record['humidity'], 2),
                    'pressure' => round($normalizedPressure, 2),
                    'sensor_id' => $sensor->id,
                    'measurement_time' => date('Y-m-d H:i:s', $record['measurement_time']),
                    'created_at' => Carbon::now()
                );
                array_push($insertData, $preparedData);
            }

            DB::table('measurements')->insert($insertData);

            return response()->json('ok');
        }

        return response()->json($errors);
    }

    public function getLast($recordsNumber, $sensor_id)
    {
        $response = new Response();

        $sensor_id=$sensor_id==0 ? DB::table('sensors')->min('id'): $sensor_id;

        if(is_null(DB::table('sensors')->where('id', $sensor_id)->get()->last())){
            $response->push_error('sensor_not_found_error');
            return $response->get();
        }

        $response->push_data(
            array_reverse(
                DB::table('measurements')
                    ->where('sensor_id', $sensor_id)
                    ->latest('measurement_time')
                    ->limit($recordsNumber)
                    ->get(['temperature', 'humidity', 'pressure', 'measurement_time as time'])
                    ->toArray()
            ),
            'result'
        );

        return $response->get();
    }

    public function getEveryHour($recordsNumber, $sensor_id)
    {
        $response = new Response();

        $sensor_id=$sensor_id==0 ? DB::table('sensors')->min('id'): $sensor_id;

        if(is_null(DB::table('sensors')->where('id', $sensor_id)->get()->last())){
            $response->push_error('sensor_not_found_error');
            return $response->get();
        }

        $response->push_data(
            array_reverse(
                DB::table('measurements')
                    ->select(
                        [
                            DB::raw('concat(date(measurement_time), " ", lpad(hour(measurement_time), 2, 0), ":00:00") as time'),
                            DB::raw('avg(temperature) as temperature'),
                            DB::raw('avg(humidity) as humidity'),
                            DB::raw('avg(pressure) as pressure'),
                            DB::raw('min(temperature) as min_temperature'),
                            DB::raw('min(humidity) as min_humidity'),
                            DB::raw('min(pressure) as min_pressure'),
                            DB::raw('max(temperature) as max_temperature'),
                            DB::raw('max(humidity) as max_humidity'),
                            DB::raw('max(pressure) as max_pressure')
                        ])
                    ->where('sensor_id', $sensor_id)
                    ->groupByRaw('time')
                    ->latest('time')
                    ->limit($recordsNumber)
                    ->get()
                    ->toArray()
            ),
            'result'
        );

        return $response->get();
    }

    public function getEveryDay($recordsNumber, $sensor_id)
    {
        $response = new Response();

        $sensor_id=$sensor_id==0 ? DB::table('sensors')->min('id'): $sensor_id;

        if(is_null(DB::table('sensors')->where('id', $sensor_id)->get()->last())){
            $response->push_error('sensor_not_found_error');
            return $response->get();
        }

        $response->push_data(
            array_reverse(
                DB::table('measurements')
                    ->select(
                        [
                            DB::raw('date(measurement_time) as time'),
                            DB::raw('avg(temperature) as temperature'),
                            DB::raw('avg(humidity) as humidity'),
                            DB::raw('avg(pressure) as pressure'),
                            DB::raw('min(temperature) as min_temperature'),
                            DB::raw('min(humidity) as min_humidity'),
                            DB::raw('min(pressure) as min_pressure'),
                            DB::raw('max(temperature) as max_temperature'),
                            DB::raw('max(humidity) as max_humidity'),
                            DB::raw('max(pressure) as max_pressure')
                        ])
                    ->where('sensor_id', $sensor_id)
                    ->groupByRaw('time')
                    ->latest('time')
                    ->limit($recordsNumber)
                    ->get()
                    ->toArray()
            ),
            'result'
        );

        return $response->get();
    }

    public function getNow($sensor_id){
        $response = new Response();

        $sensor_id=$sensor_id==0 ? DB::table('sensors')->min('id'): $sensor_id;

        if(is_null(DB::table('sensors')->where('id', $sensor_id)->get()->last())){
            $response->push_error('sensor_not_found_error');
            return $response->get();
        }

        $response->push_data(
            DB::table('measurements')
//                ->select(['temperature', 'pressure', 'humidity'])
                ->where('sensor_id', $sensor_id)
                ->latest('measurement_time')
                ->limit(1)
                ->first(),
            'actual_data'
        );

        return $response->get();
    }

    //Only for tests
    public function storeFake($accessToken)
    {
        $i = 0;
        $sensor = DB::table('sensors')->where('access_token', $accessToken)->get()->last();
        while ($i < 4000) {
            $time = Carbon::now()->addMinute($i * 15);
            $inputData = [
                'temperature' => rand(0, 20),
                'humidity' => rand(0, 100),
                'pressure' => rand(980, 1080),
                'measurement_time' => $time,
                'created_at' => Carbon::now(),
                'sensor_id' => $sensor->id
            ];

            DB::table('measurements')->insert($inputData);

            $i++;
        }

        return 'ok';
    }
}
