<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    function register(Request $request)
    {
        $correct = true;
        $response = array();
        $response['error'] = array();

        if (DB::table('users')->where('email', $request->input('email'))->count() != 0) {
            array_push($response['error'], 'email_in_use_error');
            $correct = false;
        }

        if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
            array_push($response['error'], 'email_typo_error');
            $correct = false;
        }

        if (!preg_match('^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$^', $request->input('password'))) {
            array_push($response['error'], 'password_typo_error');
            $correct = false;
        }

        if ($correct) {
            $apiToken=str::random(60);
            DB::table('users')->insert([
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'api_token'=>$apiToken,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            return \response()->json([
                'api_token'=>$apiToken,
                'email'=>$request->input('email')
            ]);
        }

        return response()->json($response);
    }

    function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $api_token = Str::random(60);
            DB::table('users')
                ->where('email', $request->input('email'))
                ->update([
                    'api_token' => $api_token,
                    'updated_at' => Carbon::now()
                ]);

            return response()->json([
                    'api_token'=>$api_token,
                    'email'=>$request->input('email')
                ]);
        }

        return response()->json(['error'=>['login_credentials_error']]);
    }

    public function logout(Request $request)
    {
        DB::table('users')->where('id', $request->user()->id)->update(['api_token' => null, 'updated_at' => Carbon::now()]);
        return response()->json(true);
    }

}
