<?php

namespace App\Http\Controllers;

use App\Models\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SensorController extends Controller
{
    public function store(Request $request)
    {
        $response = array();

        $validationRules = array(
            'name' => 'required|min:5|unique:sensors',
            'address' => 'required|min:8',
            'mas_level' => 'required|integer'
        );

        $validationResult = Validator::make($request->all(), $validationRules);

        if ($validationResult->fails()) {
            return response()->json(['error' => true]);
        } else {
            $accessTokenUnique = false;
            while (!$accessTokenUnique) {
                $accessToken = Str::random(60);
                if (DB::table('sensors')->where('access_token', $accessToken)->get()->count() == 0) {
                    $accessTokenUnique = true;
                }
            }

            $sensor = [
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'mas_level' => $request->input('mas_level'),
                'user_id' => $request->user()->id,
                'access_token' => $accessToken,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
            $sensor_id = DB::table('sensors')->insertGetId($sensor);
            return response()->json([
                'access_token' => $accessToken,
                'sensor_id' => $sensor_id
            ]);
        }
    }

    public function getAll(Request $request)
    {
        $user=DB::table('users')->where('api_token', $request->bearerToken())->whereNotNull('api_token')->get()->first();

        if (is_null($user)) {
            return response()->json([
                'sensors' => DB::table('sensors')->get(['id', 'name'])
            ]);
        } else {

            return response()->json([
                'sensors' => DB::table('sensors')
                    ->where('user_id', '<>', $user->id)
                    ->get(['id', 'name']),
                'your_sensors' => DB::table('sensors')
                    ->where('user_id', '=', $user->id)
                    ->get(['id', 'name', 'address'])
            ]);
        }
    }

    public function delete(Request $request, $sensor_id)
    {
        $response = new Response();

        $sensor = DB::table('sensors')->where(['id' => $sensor_id, 'user_id' => $request->user()->id])->get();
        if ($sensor->count() == 1) {
            DB::table('sensors')->delete($sensor_id);
            $response->push_data('sensor_deleted_successful');
        } else {
            return redirect()->route('unauthorized');
        }

        return $response->get();
    }
}
